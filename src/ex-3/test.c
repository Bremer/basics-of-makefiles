#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include "add.h"
#include "sub.h"
#include "mul.h"
#include "div.h"

void test_add(void)
{
    CU_ASSERT(add(2,2) == 4);
}

void test_sub(void)
{
    CU_ASSERT(sub(2,2) == 0);
}
void test_mul(void)
{
    CU_ASSERT(mul(2,2) == 4);
}
void test_div(void)
{
    CU_ASSERT(div(2,2) == 1);
}

int main() {
    CU_initialize_registry();
    CU_pSuite suite = CU_add_suite("maxi_test", 0, 0);

    CU_add_test(suite, "maxi_test", test_add);
    CU_add_test(suite, "maxi_test", test_sub);
    CU_add_test(suite, "maxi_test", test_mul);
    CU_add_test(suite, "maxi_test", test_div);

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();

    return 0;
}
