#include <stdio.h>
#include "add.h"
#include "sub.h"
#include "mul.h"
#include "div.h"

int main()
{
    int a = add(2, 2);
    int b = sub(2, 2);
    int c = mul(2, 2);
    int d = div(2, 2);

    printf("A: %i\n", a);
    printf("B: %i\n", b);
    printf("C: %i\n", c);
    printf("D: %i\n", d);

    return 0;
}

